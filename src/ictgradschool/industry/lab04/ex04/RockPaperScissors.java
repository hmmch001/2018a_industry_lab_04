package ictgradschool.industry.lab04.ex04;

import ictgradschool.industry.lab04.ex03.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    public static final int SCISSORS = 2;
    public static final int PAPER = 3;
    public static final int QUIT = 4;

    private String playerName;
    private int playerChoice;



    public void start() {
        System.out.println("Hi! what is your name? ");
        playerName = Keyboard.readInput();
        while(playerChoice != QUIT){
            System.out.println("1. Rock");
            System.out.println("2. Scissors");
            System.out.println("3. Paper");
            System.out.println("4. Quit");
            System.out.println("Enter choice: ");
            playerChoice = Integer.parseInt(Keyboard.readInput());
            if(playerChoice == QUIT){
                break;
            }
            int computerChoice = (int)((Math.random() * 3)+1);
            displayPlayerChoice(playerName, playerChoice);
            displayPlayerChoice("Computer", computerChoice);
            if(userWins(playerChoice,computerChoice)){
                System.out.println(playerName + " wins because " + getResultString(playerChoice,computerChoice ));
            }else if(!userWins(playerChoice,computerChoice) && !getResultString(playerChoice, computerChoice).equals("No one wins.")){
                System.out.println("The computer wins because " + getResultString(playerChoice, computerChoice ));
            }else {
                System.out.println(getResultString(playerChoice, computerChoice ));
            }
        }
        System.out.println("Goodbye " + playerName + ". Thanks for playing :)");




    }


    public void displayPlayerChoice(String playerName, int playerChoice) {
        String c = "";
        if(playerChoice == ROCK){
            c = "rock.";
        }else if(playerChoice == SCISSORS){
            c = "scissors.";
        }else if (playerChoice == PAPER){
            c = "paper.";
        }
        System.out.println(playerName + " chose " + c);


    }


    public boolean userWins(int playerChoice, int computerChoice) {
        if (playerChoice== PAPER && computerChoice == ROCK || playerChoice== ROCK && computerChoice == SCISSORS || playerChoice== SCISSORS && computerChoice == PAPER){
            return true;
        }
        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {
        String result = "";
        final String PAPER_WINS = "paper covers rock.";
        final String ROCK_WINS = "rock smashes scissors.";
        final String SCISSORS_WINS = "scissors cut paper.";
        final String TIE = "No one wins.";
        if(playerChoice == computerChoice){
            result = TIE;
        }else if((playerChoice == PAPER || computerChoice == PAPER)&&(playerChoice == ROCK || computerChoice == ROCK)){
            result = PAPER_WINS;
        }else if((playerChoice == ROCK || computerChoice == ROCK)&&(playerChoice == SCISSORS || computerChoice == SCISSORS)){
            result = ROCK_WINS;
        }else if((playerChoice == SCISSORS || computerChoice == SCISSORS)&&(playerChoice == PAPER || computerChoice == PAPER)){
            result = SCISSORS_WINS;
        }

        return result;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
