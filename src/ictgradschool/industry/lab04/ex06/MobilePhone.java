package ictgradschool.industry.lab04.ex06;

public class MobilePhone {


    private String brand;
    private String model;
    private double price;
    
    public MobilePhone(String brand, String model, double price) {
        this.brand = brand;
        this.model = model;
        this.price = price;
    }



    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }


    public String toString() {
        String info = brand + " " + model + " which cost $" + price;
        return info;
    }

    public boolean isCheaperThan(MobilePhone other) {
        if(this.price < other.price){
            return true;
        }
        return false;
    }
    
    public boolean equals(MobilePhone other){
        if(this.brand.equals(other.brand) && this.model.equals(other.model)){
            return true;
        }
        return false;
    }



}


