package ictgradschool.industry.lab04.ex05;

public class Pattern {
    private int numberOfCharacters;
    private char characterType;
    public Pattern( int number,  char type) {
        this.numberOfCharacters = number;
        this.characterType = type;
    }
    public String toString(){
        String info= "";
        for(int j = 0; j< numberOfCharacters; j ++){
            info = info + characterType;

        }
        return info;

    }

    public int getNumberOfCharacters(){
        return numberOfCharacters;
    }

    public void setNumberOfCharacters(int number){
        this.numberOfCharacters = number;
    }




}

